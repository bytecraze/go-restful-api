module gitlab.com/bytecraze/go-restful-api

go 1.12

require (
	github.com/emicklei/go-restful v2.11.1+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/kr/pretty v0.1.0 // indirect
	github.com/rs/xid v1.2.1
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
